unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Length: TButton;
    contains: TButton;
    trim: TButton;
    LowerCase: TButton;
    UpperCase: TButton;
    Replace: TButton;
    Adicionar: TButton;
    Apagar: TButton;
    Memo1: TMemo;
    Edit1: TEdit;
    Memo2: TMemo;
    procedure AdicionarClick(Sender: TObject);
    procedure ApagarClick(Sender: TObject);
    procedure LengthClick(Sender: TObject);
    procedure LowerCaseClick(Sender: TObject);
    procedure UpperCaseClick(Sender: TObject);
    procedure ReplaceClick(Sender: TObject);
    procedure containsClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  lc: string;

implementation

{$R *.dfm}

procedure TForm1.AdicionarClick(Sender: TObject);
begin
Memo1.text := Edit1.text;
end;

procedure TForm1.ApagarClick(Sender: TObject);
begin
Memo1.clear;
end;

procedure TForm1.containsClick(Sender: TObject);
var conteudo: string;
begin
conteudo := memo1.text;
memo2.Text := conteudo;
if  conteudo.Contains('ola') then
  begin
  Memo2.text:= ('tem "ola"');
  end
  else
  begin
    Memo2.text:=('nao tem "ola"');
end;

end;

procedure TForm1.LengthClick(Sender: TObject);
var conteudo : string;
begin
  conteudo := Memo1.Text;
  Memo2.Text := conteudo.Length.ToString;
end;

procedure TForm1.LowerCaseClick(Sender: TObject);
begin
 lc := Memo1.Text;
 Memo2.text := lc.LowerCase(lc);

end;

procedure TForm1.ReplaceClick(Sender: TObject);
var troca: string;
begin
troca:= Memo1.Text;
Memo2.Text:= troca.Replace('', 'TOP');

end;

procedure TForm1.UpperCaseClick(Sender: TObject);
begin
 lc := Memo1.Text;
 Memo2.text := lc.UpperCase(lc);
end;

end.
